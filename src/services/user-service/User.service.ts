import axios from "axios";
import { User } from ".";
import { BASE_URL } from "../consts";

class UserService {
  public getUsers = async () => {
    try {
      const result = await axios.get<User[]>(`${BASE_URL}users`);
      return result.data;
    } catch (err) {
      console.error(err);
    }
  };

  public getUser = async (id: string) => {
    try {
      const result = await axios.get<User>(`${BASE_URL}users/${id}`);
      return result.data;
    } catch (err) {
      console.error(err);
    }
  }
}

export const userService = new UserService();
