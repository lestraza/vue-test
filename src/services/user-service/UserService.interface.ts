export interface User {
  email: string;
  id: number;
  name: string;
  phone: string;
  username: string;
  website: string;
  address: UserAddress;
  company: UserCompany;
}

export interface UserAddress {
  city: string;
  geo: { lat: string; lng: string };
  lat: string;
  lng: string;
  street: string;
  suite: string;
  zipcode: string;
}

export interface UserCompany {
  name: string;
  catchPhrase: string;
  bs: string;
}
