import axios from "axios";
import { Post } from ".";
import { BASE_URL } from "../consts";

class PostService {
  public getPosts = async () => {
    try {
      const result = await axios.get<Post[]>(`${BASE_URL}posts`);
      return result.data;
    } catch (error) {
      console.error(error);
    }
  };
}

export const postService = new PostService();
