export interface IMenuItem {
    label: string
    path: string
}

export const menu: IMenuItem[] = [
    {
        label: 'Users',
        path: '/users',
    },
    {
        label: 'Posts',
        path: '/posts',
    },
]