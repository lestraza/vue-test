import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Users from "../pages/users/Users.page.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: '/users',
    name: 'Users',
    component: Users
  },
  {
    path: '/users/:id',
    name: 'User Details',
    component: () => import('../pages/user-details/UserDetails.page.vue')
  },
  {
    path: '/posts',
    name: 'Posts',
    component: () => import('../pages/posts/Posts.page.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
