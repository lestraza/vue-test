export interface IColumn<T> {
  header: string
  accessor: keyof T
  width?: string | number
}
