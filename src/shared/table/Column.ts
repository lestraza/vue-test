import { IColumn } from './Table.interface'

export class Column<T> implements IColumn<T> {
  constructor(
    public header: string,
    public accessor: keyof T,
    public width?: string | number,
  ) {}

  public static createColumns<T>(columns: Column<T>[]) {
    return columns.map(
      ({ accessor, header, width }) => new Column(header, accessor, width),
    )
  }
}
