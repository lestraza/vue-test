import { Column } from '@/shared/table/Column'
import { User } from './../../services/user-service/UserService.interface'

export const userColumns = Column.createColumns<User>([
  {
    header: 'Name',
    accessor: 'name',
  },
  {
    header: 'Email',
    accessor: 'email',
  },
  {
    header: 'Phone',
    accessor: 'phone',
  },
  {
    header: 'Website',
    accessor: 'website',
  },
])
