import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import icons from './icons'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

icons.forEach(icon => library.add(icon))

const app = createApp(App)

app.component('fa-icon', FontAwesomeIcon)
app.use(router)
app.mount('#app')
