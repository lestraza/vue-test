import { faEnvelope, faPhone, faGlobe } from '@fortawesome/free-solid-svg-icons'

export default [faEnvelope, faPhone, faGlobe]
